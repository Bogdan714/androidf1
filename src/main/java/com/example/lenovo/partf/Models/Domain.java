package com.example.lenovo.partf.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class Domain implements Parcelable {
    private String name;
    private String countries;

    public String getName() {
        return name;
    }

    public String getCountries() {
        return countries;
    }

    public Domain(Parcel in) {
        name = in.readString();
        countries = in.readString();
    }

    public Domain(String name) {
        this.name = name;
        this.countries = "";
    }

    public Domain(String name, String first) {
        this.name = name;
        this.countries = first;
    }

    public void addCountry(String country) {
        this.countries += ", " + country;
    }

    public boolean containsQuery(String query) {
        return (name.toLowerCase().contains(query.toLowerCase())
                || countries.toLowerCase().contains(query.toLowerCase()));
    }

    public static final Parcelable.Creator<Domain> CREATOR = new Parcelable.Creator<Domain>() {
        @Override
        public Domain createFromParcel(Parcel in) {
            return new Domain(in);
        }

        @Override
        public Domain[] newArray(int size) {
            return new Domain[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(countries);
    }

}
