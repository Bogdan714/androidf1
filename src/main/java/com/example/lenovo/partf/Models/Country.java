package com.example.lenovo.partf.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class Country implements Parcelable {
    private String name;
    private String capital;
    private Long population;
    private String region;
    private String subregion;
    private String alpha3Code;

    public Country(String name, String capital, Long population, String region, String subregion, String alpha3Code) {
        this.name = name;
        this.capital = capital;
        this.population = population;
        this.region = region;
        this.subregion = subregion;
        this.alpha3Code = alpha3Code;
    }

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }

    public Long getPopulation() {
        return population;
    }

    public String getRegion() {
        return region;
    }

    public String getSubregion() {
        return subregion;
    }

    public String getAlpha3Code() {
        return alpha3Code;
    }

    public static Creator<Country> getCREATOR() {
        return CREATOR;
    }

    public boolean containsQuery(String query){
        return (name.toLowerCase().contains(query.toLowerCase())
                ||capital.toLowerCase().contains(query.toLowerCase())
                ||subregion.toLowerCase().contains(query.toLowerCase()));
    }

    protected Country(Parcel in) {
        name = in.readString();
        capital = in.readString();
        population = in.readLong();
        region = in.readString();
        subregion = in.readString();
        alpha3Code = in.readString();
    }

        public static final Parcelable.Creator<Country> CREATOR = new Parcelable.Creator<Country>() {
            @Override
            public Country createFromParcel(Parcel in) {
                return new Country(in);
            }

            @Override
            public Country[] newArray(int size) {
                return new Country[size];
            }
        };

        @Override
        public int describeContents() {
        return 0;
    }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(name);
            dest.writeString(capital);
            dest.writeLong(population);
            dest.writeString(region);
            dest.writeString(subregion);
            dest.writeString(alpha3Code);
    }
}
