package com.example.lenovo.partf;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.lenovo.partf.Models.Domain;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutDomainActivity extends AppCompatActivity {
    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.countries)
    TextView countries;

    Domain domain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_domain);
        ButterKnife.bind(this);

        if (getIntent() != null && getIntent().getParcelableExtra(ListActivity.KEY_LABEL_2) != null) {
            domain = getIntent().getParcelableExtra(ListActivity.KEY_LABEL_2);
            name.setText(domain.getName());
            countries.setText("Countries: " + domain.getCountries() + ".");
        }
    }
}
