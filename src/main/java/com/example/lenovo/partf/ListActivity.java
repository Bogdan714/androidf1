package com.example.lenovo.partf;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.lenovo.partf.Models.Country;
import com.example.lenovo.partf.Models.Domain;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import butterknife.OnTextChanged;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ListActivity extends AppCompatActivity {

    public static final String KEY_LABEL_1 = "key1";
    public static final String KEY_LABEL_2 = "key2";

    @BindView(R.id.search_bar)
    EditText searchBar;

    ListAdapter listAdapter;
    DomainListAdapter domainListAdapter;

    List<Country> countryList;
    List<Domain> domainList;

    String mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        ButterKnife.bind(this);
        ListView listView = findViewById(R.id.list);

        if (getIntent() != null && getIntent().getStringExtra(MainActivity.MODE_KEY) != null) {
            mode = getIntent().getStringExtra(MainActivity.MODE_KEY);
        }

        countryList = new ArrayList<>();
        domainList = new ArrayList<>();

        switch (mode) {
            case "show_countries":
                listAdapter = new ListAdapter(this, countryList);
                listView.setAdapter(listAdapter);
                generateCountries();

                break;
            case "show_subregions":
                domainListAdapter = new DomainListAdapter(this, domainList);
                listView.setAdapter(domainListAdapter);
                generateSubregions();
                break;
            case "show_regions":
                domainListAdapter = new DomainListAdapter(this, domainList);
                listView.setAdapter(domainListAdapter);
                generateRegions();
                break;
            default:
                Toast.makeText(ListActivity.this, "error, no mode", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void generateCountries() {
        Retrofit.getCountries(new Callback<List<Country>>() {

            @Override
            public void success(List<Country> countries, Response response) {
                listAdapter.addAll(countries);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(ListActivity.this, "failure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void generateRegions() {

        Retrofit.getCountries(new Callback<List<Country>>() {

            @Override
            public void success(List<Country> countries, Response response) {

                List<Domain> list = new ArrayList<>();
                boolean hasToAdd;
                for (int i = 0; i < countries.size(); i++) {
                    hasToAdd = true;
                    for (int j = 0; j < list.size(); j++) {
                        if (list.get(j).getName().equals(countries.get(i).getRegion())) {
                            hasToAdd = false;
                            list.get(j).addCountry(countries.get(i).getName());
                        }
                    }
                    if (hasToAdd) {
                        list.add(new Domain(countries.get(i).getRegion(), countries.get(i).getName()));
                    }
                }
                domainListAdapter.addAll(list);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(ListActivity.this, "failure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void generateSubregions() {
        Retrofit.getCountries(new Callback<List<Country>>() {
            @Override
            public void success(List<Country> countries, Response response) {

                List<Domain> list = new ArrayList<>();
                boolean hasToAdd;
                for (int i = 0; i < countries.size(); i++) {
                    hasToAdd = true;
                    for (int j = 0; j < list.size(); j++) {
                        if (list.get(j).getName().equals(countries.get(i).getSubregion())) {
                            hasToAdd = false;
                            list.get(j).addCountry(countries.get(i).getName());
                        }
                    }
                    if (hasToAdd) {
                        list.add(new Domain(countries.get(i).getSubregion(), countries.get(i).getName()));
                    }
                }
                domainListAdapter.addAll(list);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(ListActivity.this, "failure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnItemClick(R.id.list)
    public void onItemClick(int position) {
        Intent intent;
        if (listAdapter != null) {
            intent = new Intent(this, AboutCountryActivity.class);
            intent.putExtra(KEY_LABEL_1, listAdapter.getItem(position));
            startActivity(intent);
        }
        if (domainListAdapter != null) {
            intent = new Intent(this, AboutDomainActivity.class);
            intent.putExtra(KEY_LABEL_2, domainListAdapter.getItem(position));
            startActivity(intent);
        }
    }

    @OnTextChanged(R.id.search_bar)
    public void onTextChanged() {
        if (domainListAdapter != null) {
            domainListAdapter.searchUpdate(searchBar.getText().toString());
        }
        if (listAdapter != null) {
            listAdapter.searchUpdate(searchBar.getText().toString());
        }
    }
}
