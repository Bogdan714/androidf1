package com.example.lenovo.partf;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.lenovo.partf.Models.Country;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListAdapter extends BaseAdapter {

    private Context context;
    private List<Country> countries;
    private List<Country> countriesCopy;

    public ListAdapter(Context context, List<Country> countries) {
        this.context = context;
        this.countries = countries;
        countriesCopy = new ArrayList<>();
        countriesCopy.addAll(countries);
    }

    public void update() {
        notifyDataSetChanged();
    }

    public void addAll(List<Country> countryList) {
        countries.addAll(countryList);
        countriesCopy.addAll(countries);
        notifyDataSetChanged();
    }

    public void searchUpdate(String query) {
        countries.clear();
        countries.addAll(countriesCopy);
        if (!query.equals("")) {
            for (int i = countries.size() - 1; i >= 0; i--) {
                if (!countries.get(i).containsQuery(query)) {
                    countries.remove(i);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return countries.size();
    }

    @Override
    public Country getItem(int position) {
        return countries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return countries.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder holder;
        if (rowView == null) {
            rowView = LayoutInflater.from(context)
                    .inflate(R.layout.list_node, parent, false);
            holder = new ViewHolder(rowView);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }


        holder.info.setText(getItem(position).getName());
        return rowView;
    }

    static class ViewHolder {

        @BindView(R.id.node)
        TextView info;

        public ViewHolder(View root) {
            ButterKnife.bind(this, root);
        }
    }
}
