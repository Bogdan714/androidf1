package com.example.lenovo.partf;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.lenovo.partf.Models.Country;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutCountryActivity extends AppCompatActivity {

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.capital)
    TextView capital;

    @BindView(R.id.population)
    TextView population;

    @BindView(R.id.region)
    TextView region;

    @BindView(R.id.subregion)
    TextView subregion;

    @BindView(R.id.code3)
    TextView code3;

    Country country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_country);
        ButterKnife.bind(this);

        if (getIntent() != null && getIntent().getParcelableExtra(ListActivity.KEY_LABEL_1) != null) {
            country = getIntent().getParcelableExtra(ListActivity.KEY_LABEL_1);
            name.setText(country.getName());
            capital.setText("Capital: " + country.getCapital());
            population.setText("Population: " + country.getPopulation());
            region.setText("Region: " + country.getRegion());
            subregion.setText("Subregion: " + country.getSubregion());
            code3.setText("Alpha3Code: " + country.getAlpha3Code());
        }
    }
}
