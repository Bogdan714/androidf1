package com.example.lenovo.partf;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.lenovo.partf.Models.Domain;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DomainListAdapter extends BaseAdapter {
    private Context context;
    private List<Domain> domains;
    private List<Domain> domainsCopy;

    public DomainListAdapter(Context context, List<Domain> domains) {
        this.context = context;
        this.domains = domains;
        this.domainsCopy = new ArrayList<>();
        domainsCopy.addAll(domains);
    }

    public void addAll(List<Domain> domainList) {
        domains.addAll(domainList);
        domainsCopy.addAll(domains);
        notifyDataSetChanged();
    }

    public void searchUpdate(String query) {
        domains.clear();
        domains.addAll(domainsCopy);
        if (!query.equals("")) {
            for (int i = domains.size() - 1; i >= 0; i--) {
                if (!domains.get(i).containsQuery(query)) {
                    domains.remove(i);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return domains.size();
    }

    @Override
    public Domain getItem(int position) {
        return domains.get(position);
    }

    @Override
    public long getItemId(int position) {
        return domains.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ListAdapter.ViewHolder holder;
        if (rowView == null) {
            rowView = LayoutInflater.from(context)
                    .inflate(R.layout.list_node, parent, false);
            holder = new ListAdapter.ViewHolder(rowView);
            rowView.setTag(holder);
        } else {
            holder = (ListAdapter.ViewHolder) rowView.getTag();
        }


        holder.info.setText(getItem(position).getName());
        return rowView;
    }

    static class ViewHolder {

        @BindView(R.id.node)
        TextView info;

        public ViewHolder(View root) {
            ButterKnife.bind(this, root);
        }
    }
}
