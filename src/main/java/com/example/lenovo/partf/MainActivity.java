package com.example.lenovo.partf;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    public static final String MODE_KEY = "mode";
    private static final String[] MODES = {"show_countries", "show_subregions", "show_regions"};
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        intent = new Intent(this, ListActivity.class);
    }

    @OnClick(R.id.countries)
    public void onCountriesClicked() {
        intent.putExtra(MODE_KEY, MODES[0]);
        startActivity(intent);
    }

    @OnClick(R.id.regions)
    public void onSubregionsClicked() {
        intent.putExtra(MODE_KEY, MODES[1]);
        startActivity(intent);
    }

    @OnClick(R.id.world_parts)
    public void onRegionsClicked() {
        intent.putExtra(MODE_KEY, MODES[2]);
        startActivity(intent);
    }
}
